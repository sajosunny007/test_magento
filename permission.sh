#!/bin/bash
#
sudo rm -rf var/* generated/* pub/static/*
sudo chmod 777 -R var generated pub/static

php bin/magento cache:flush
sudo chmod 777 -R var generated pub/static

php bin/magento cache:clean
sudo chmod 777 -R var generated pub/static

php bin/magento indexer:reindex
sudo chmod 777 -R var generated pub/static
php bin/magento setup:upgrade

sudo chmod 777 -R var generated pub/static
php bin/magento setup:static-content:deploy -f

sudo chmod 777 -R var generated pub/static
